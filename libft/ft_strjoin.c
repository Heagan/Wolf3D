/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 16:29:56 by gsferopo          #+#    #+#             */
/*   Updated: 2017/06/08 16:31:42 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	char	*nnews;

	i = -1;
	j = -1;
	if (s1 == NULL || s2 == NULL)
		return (NULL);
	nnews = ft_strnnew(ft_strlen(s1) + ft_strlen(s2));
	if (nnews == NULL)
		return (NULL);
	while (s1[++i] != '\0')
		nnews[i] = s1[i];
	while (s2[++j] != '\0')
	{
		nnews[i] = s2[j];
		i++;
	}
	nnews[i] = '\0';
	return (nnews);
}
