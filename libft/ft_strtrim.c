/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 09:44:39 by gsferopo          #+#    #+#             */
/*   Updated: 2017/07/24 13:48:58 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	getse(char const *s, int i, int *start, int *end)
{
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	*start = i;
	while ((s[*end] == ' ' || s[*end] == '\n' || s[*end] == '\t' ||
				s[*end] == '\0') && *end > 0)
		(*end)--;
}

char		*ft_strtrim(char const *s)
{
	char	*nnews;
	int		i;
	int		start;
	int		end;

	i = 0;
	if (s == NULL)
		return (NULL);
	if (s[0] == '\0')
		return ((char *)s);
	end = ft_strlen(s);
	getse(s, i, &start, &end);
	start--;
	i = -1;
	nnews = ft_strnnew(ft_clamp(1, end - start, end - start));
	if (nnews == NULL)
		return (NULL);
	if (end == 0 && (nnews[0] = '\0') != '\t')
		return (nnews);
	while (++start < end + 1)
		nnews[++i] = s[start];
	nnews[end - start] = '\0';
	return (nnews);
}
