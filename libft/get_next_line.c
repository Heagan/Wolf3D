/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 12:56:24 by bmoodley          #+#    #+#             */
/*   Updated: 2018/01/22 07:44:53 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	build(char **line, char **nnew, char *pos)
{
	*line = ft_strsub(*nnew, 0, ft_strlen(*nnew) - ft_strlen(pos));
	*nnew = ft_strdup(pos + 1);
}

static int	ft_append(int fd, char **nnew)
{
	char		*buf;
	char		*temp;
	int			ret;

	buf = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1));
	if (buf == NULL)
		return (-1);
	ret = read(fd, buf, BUFF_SIZE);
	if (ret > 0)
	{
		buf[ret] = '\0';
		temp = ft_strjoin(*nnew, buf);
		free(*nnew);
		*nnew = temp;
	}
	free(buf);
	return (ret);
}

int			get_next_line(const int fd, char **line)
{
	static char *nnew = NULL;
	char		*pos;
	int			ret;

	if (nnew == NULL)
		nnew = ft_strnnew(0);
	pos = ft_strchr(nnew, '\n');
	while (pos == NULL)
	{
		ret = ft_append(fd, &nnew);
		if (ret == 0)
		{
			if (ft_strlen(nnew) == 0)
				return (0);
			nnew = ft_strjoin(nnew, "\n");
		}
		if (ret < 0)
			return (-1);
		else
			pos = ft_strchr(nnew, '\n');
	}
	build(line, &nnew, pos);
	return (1);
}
