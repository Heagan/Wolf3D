/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 11:51:33 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/13 11:52:16 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_count_list(void *l)
{
	t_lst	*ll;

	ll = (t_lst *)l;
	if (ll == NULL)
		return (0);
	return (1 + ft_count_list(ll->next));
}