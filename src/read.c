#include "wolf3d.h"

//debug
void		ft_lstprint(t_env *env)
{
	t_read	*current;

	current = env->head;
	while (current)
	{
		printf("1(%d; %d)\t2(%d; %d)\n", current->x0, current->y0, current->x1, current->y1);
		current = current->next;
	}
}

t_read		*lstnnew(int x, int y)
{
	t_read	*nnew;

	nnew = (t_read *)malloc(sizeof(t_read));
	if (nnew != NULL)
	{
		nnew->x0 = x * SCALE;
		nnew->y0 = y * SCALE;
		nnew->x1 = nnew->x0 + SCALE;
		nnew->y1 = nnew->y0 + SCALE;
		nnew->next = NULL;
	}
	return (nnew);
}

void		lstadd(t_read **head, int x, int y)
{
	t_read	*current;

	current = *head;
	if (current != NULL)
	{
		while (current->next)
			current = current->next;
		current->next = lstnnew(x, y);
	}
	else
		*head = lstnnew(x, y);
}

int			ft_read(t_env *env)
{
	int		x;
	int		i;
	int		cnt;
	char	*line;

	env->head = NULL;
	line = NULL;
	cnt = -1;
	x = -1;
	// if (!(env->fd > 0))
	// 	while (++x < 17)
	// 	{
	// 		if (x == 0 || x == 16)
	// 			line = "11111111111111111";
	// 		else if ( x == 8)
	// 			line = "10000000100000001";
	// 		else
	// 			line = "10000000000000001";
	// 		i = -1;
	// 		++cnt;
	// 		while (line[++i] != '\0')
	// 			if (line[i] != '0')// && ft_isdigit(line[i]))
	// 				lstadd(&(env->head), i, cnt);
	// 	}
	if (env->fd > 0)
		while (get_next_line(env->fd, &line) > 0)
		{
			i = -1;
			++cnt;
			while (line[++i] != '\0')
			{
				if (line[i] == '1' && ft_isdigit(line[i]))
					lstadd(&(env->head), i, cnt);
				if (line[i] == 'x' || line[i] == 'X')
				{
					env->plr.x = i * SCALE;
					env->plr.y = cnt * SCALE;
				}
				if (env->map_x == SCREEN_WIDTH || env->map_x < i * SCALE)
					env->map_x = i * SCALE;
			}
			free(line);
			line = NULL;
		}
	env->map_y = cnt * SCALE;
	return (0);
}

