#include "wolf3d.h"


void	ft_mainloop(t_env *env)
{
	int		quit;

	quit = 0;
	while (!quit)
	{
		while (SDL_PollEvent(&env->ev))
		{
			if (env->ev.type == SDL_QUIT)
				quit = 1;
			if (env->ev.type == SDL_KEYDOWN)
				ft_keyhandler(env);
		}
		clearpixels(env->sur);
		clearpixels(env->map_sur);
		mini_map(env);
		ray_caster(env);
		SDL_UpdateWindowSurface(env->win);
		SDL_UpdateWindowSurface(env->win_map);
	}
}

int		main(int argc, char **argv)
{
	t_env env;

	env.plr.pov = 225;
	env.plr.fov = 35;
	env.plr.x = 50;
	env.plr.y = 50;
	env.map_x = SCREEN_WIDTH;
	env.map_y = SCREEN_HEIGHT;
	env.fd = open(argv[1], O_RDONLY);
	if (env.fd == -1)
		env.fd = open("maps/wolf.map", O_RDONLY);
	printf("%d %s\n", env.fd, argv[1]);
	ft_read(&env);
	printf("%i %i\n", env.map_x, env.map_y);
	SDL_Init(SDL_INIT_EVERYTHING);
	env.win_map = SDL_CreateWindow("Wolf Map", 50, 50, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	env.win = SDL_CreateWindow("Wolf3D", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	env.sur = SDL_GetWindowSurface(env.win);
	env.map_sur = SDL_GetWindowSurface(env.win_map);
	ft_mainloop(&env);
	SDL_DestroyWindow(env.win);
	SDL_DestroyWindow(env.win_map);
	SDL_Quit();
	return (0);
}
