#include "wolf3d.h"

double			sqr(double n)
{
	return (n * n);
}

void			swap(double *a, double *b)
{
	double		c;

	c = *a;
	*a = *b;
	*b = c;
}

double			intersect(t_env *e, t_read *w)
{
	double		a;
	double		c;
	double		t;
	double		tmin;
	double		tmax;
	double		txmin;
	double		txmax;
	double		tymin;
	double		tymax;

	a = DEG_TO_RAD(e->plr.a);
	txmin = (double)(w->x0 - e->plr.x) / sin(a);
	tymin = (double)(w->y0 - e->plr.y) / cos(a);
	txmax = (double)(w->x1 - e->plr.x) / sin(a);
	tymax = (double)(w->y1 - e->plr.y) / cos(a);

	if (txmin > txmax)
		swap(&txmin, &txmax);
	if (tymin > tymax)
		swap(&tymin, &tymax);

	if ((txmin > tymax) || (tymin > txmax))
		return (0);

	tmin = txmin;
	tmax = txmax;

	if (tymin > txmin)
		tmin = tymin;

	if (tymax > txmax)
		tmax = tymax;
	
	if (tmax < tmin)
		t = tmax;
	else
		t = tmin;

	e->plr.hitWall.x = w->x0;
	e->plr.hitWall.t = w->y0;

	return (t);
}

int				get_wall_color(t_env *e, double t, t_read *r)
{
	double		txmin;
	double		txmax;
	double		tymin;
	double		tymax;

	while (r)
	{
		txmin = (double)(r->x0 - e->plr.x) / SIN(e->plr.a);
		txmax = (double)(r->x1 - e->plr.x) / SIN(e->plr.a);
		tymin = (double)(r->y0 - e->plr.y) / COS(e->plr.a);
		tymax = (double)(r->y1 - e->plr.y) / COS(e->plr.a);

		if (txmin > txmax)
			swap(&txmin, &txmax);
		if (tymin > tymax)
			swap(&tymin, &tymax);

		if (t == txmin)
			return (0);
		if (t == txmax)
			return (1);
		if (t == tymin)
			return (2);
		if (t == tymax)
			return (3);

		r = r->next;
	}
	return (-1);
}



double			collision(t_env *e)
{
	t_read		*r;
	double		dis;
	double		c_dis;

	c_dis = 0;
	e->plr.t = -1;
	r = e->head;
	while (r)
	{
		if ((dis = intersect(e, r)) > 0)
			if (dis < c_dis || c_dis == 0)
				c_dis = dis;
		r = r->next;
	}
	e->plr.t = get_wall_color(e, c_dis, e->head);
	if (c_dis != 0)
		return (c_dis);
	return (0);
}

void		ray_caster(t_env *e)
{
	double	dis;
	t_draw	d;
	double	rayStep;
	double	tempA;

	e->plr.s = -1;
	e->plr.a = e->plr.pov - (e->plr.fov / 2);
	rayStep = (double)e->plr.fov / SCREEN_WIDTH;
	while (++e->plr.s < SCREEN_WIDTH)
	{
		d.x1 = e->plr.x;
		d.y1 = e->plr.y;
		if ((dis = collision(e)) > 0)
		{
			d.x2 = e->plr.x + SIN(e->plr.a) * dis;
			d.y2 = e->plr.y + COS(e->plr.a) * dis;
			draw_line(e->map_sur, d, 0xFFFFFF);
			d.x2 = e->plr.x + SIN(e->plr.pov) * dis;
			d.y2 = e->plr.y + COS(e->plr.pov) * dis;
			draw_line(e->map_sur, d, 0xFF0000);
			draw_wall(e, dis, e->plr.s, e->plr.a);
		}
		e->plr.a += rayStep;
	}
}