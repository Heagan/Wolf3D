#include "wolf3d.h"

void		ft_keyhandler(t_env *e)
{
	double	dis;

	if (KEYSYM == SDLK_ESCAPE)
		exit(0);
	if (KEYSYM == SDLK_UP || KEYSYM == SDLK_DOWN)
	{
		e->plr.a = (KEYSYM != SDLK_DOWN) ? e->plr.pov : (180 - e->plr.pov);
		dis = collision(e);
		e->plr.x += (KEYSYM != SDLK_DOWN) ? SIN(POV) * MOVESPD : -SIN(POV) * MOVESPD;
		e->plr.y += (KEYSYM != SDLK_DOWN) ? COS(POV) * MOVESPD : -COS(POV) * MOVESPD;
		if (dis <= MOVESPD && dis > 0)
		{
			e->plr.x += (KEYSYM != SDLK_DOWN) ? -SIN(POV) * MOVESPD : SIN(POV) * MOVESPD;
			e->plr.y += (KEYSYM != SDLK_DOWN) ? -COS(POV) * MOVESPD : COS(POV) * MOVESPD;
		}
	}
	else if (KEYSYM == SDLK_LEFT || KEYSYM == SDLK_RIGHT)
	{
		POV += (KEYSYM != SDLK_RIGHT) ? -TURNRATE : (TURNRATE);
		if (POV > 360)
			POV = TURNRATE;
		if (POV < 0)
			POV = 360 - TURNRATE;
	}
}
