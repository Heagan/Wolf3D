#include "wolf3d.h"

int		mkcol(int r, int g, int b)
{
	return (r * 256 * 256 + g * 256 + b);
}

void	clearpixels(SDL_Surface *e)
{
	unsigned int *pxlptr;
	int	x;
	int	y;

	pxlptr = (unsigned int *)e->pixels;
	y = -1;
	while (++y < SCREEN_HEIGHT)
	{
		x = -1;
		while (++x < SCREEN_WIDTH)
		{
			pxlptr[((int)y * SCREEN_WIDTH) + (int)x] = 0;
		}
	}
}

void	sdlputpixel(SDL_Surface *e, int x, int y, int color)
{
	unsigned int *pxlptr;
	int			lineoffset;

	if (y >= SCREEN_HEIGHT || y < 0)
		return;
	if (x >= SCREEN_WIDTH || x < 0)
		return;
	lineoffset = (y * SCREEN_WIDTH) + x;
	pxlptr = (unsigned int *)e->pixels;
	pxlptr[lineoffset] = color;
}

int			draw_line(SDL_Surface *e, t_draw d, int colour)
{
	d.dx = d.x2 - d.x1;
	d.dy = d.y2 - d.y1;
	d.steps = abs(d.dx) > abs(d.dy) ? abs(d.dx) : abs(d.dy);
	d.xinc = (float)d.dx / (float)d.steps;
	d.yinc = (float)d.dy / (float)d.steps;
	d.x = d.x1;
	d.y = d.y1;
	d.i = -1;
	while (++d.i <= d.steps)
	{
		sdlputpixel(e, d.x, d.y, colour);
		d.x += d.xinc;
		d.y += d.yinc;
	}
	return (1);
}

void	draw_line_wall(SDL_Surface *e, t_draw d, int colour, int size)
{
	int wall_draw_size = (double)SCALE / (d.y1) * 500;
	int start_y = (SCREEN_HEIGHT / 2) - (size / 2);
	int i = 0;

	while (i < wall_draw_size)
	{
		sdlputpixel(e, d.x, SCREEN_HEIGHT - start_y + i, colour);
		i++;
	}
}

void		draw_wall(t_env *e, double size, int loc, double a)
{
	t_draw	d;
	double	r_a;

	if (a > e->plr.pov)
		r_a = a - e->plr.pov;
	else
		r_a = e->plr.pov - a;
	size = size * COS(r_a);

	double lineHeight = (SCREEN_HEIGHT / size) * SCALE;
	double drawStart = -lineHeight / 2 + SCREEN_HEIGHT / 2;
	double drawEnd = lineHeight / 2 + SCREEN_HEIGHT / 2;

	d.x1 = loc;
	d.x2 = loc;
	d.y1 = drawStart;
	d.y2 = drawEnd;

	if (d.y2 > SCREEN_HEIGHT / 2)
		if (d.y1 < SCREEN_HEIGHT / 2)
		{
			if (e->plr.t == 0)
				draw_line(e->sur, d, 0xFF0000);
			else if (e->plr.t == 1)
				draw_line(e->sur, d, 0x00FF00);
			else if (e->plr.t == 2)
				draw_line(e->sur, d, 0x0000FF);
			else if (e->plr.t == 3)
				draw_line(e->sur, d, 0xFAFA55);
		}
}


void		mini_map(t_env *e)
{
	t_read	*r;
	t_draw	d;

	d.x1 = e->plr.x;
	d.y1 = e->plr.y;
	d.x2 = e->plr.x + SIN(e->plr.pov) * 15;
	d.y2 = e->plr.y + COS(e->plr.pov) * 15;
	//	sdlputpixel(e->map_sur, d.x1, d.y1, 0xFF0000);
	//	draw_line(e->map_sur, d, 0xFF0000);
	d.x2 = e->plr.x + SIN((POV + FOV / 2)) * 25;
	d.y2 = e->plr.y + COS((POV + FOV / 2)) * 25;
	draw_line(e->map_sur, d, 0xFF0000);
	d.x2 = e->plr.x + SIN((POV - FOV / 2)) * 25;
	d.y2 = e->plr.y + COS((POV - FOV / 2)) * 25;
	draw_line(e->map_sur, d, 0xFF0000);

	r = e->head;
	while (r)
	{
		d.x1 = r->x0;
		d.x2 = r->x0;
		d.y1 = r->y0;
		d.y2 = r->y1;
		draw_line(e->map_sur, d, 0x00FF00);
		d.y2 = r->y0;
		d.x2 = r->x1;
		draw_line(e->map_sur, d, 0x00FF00);
		d.y2 = r->y1;
		d.x1 = r->x1;
		draw_line(e->map_sur, d, 0x00FF00);
		d.x1 = r->x0;
		d.x2 = r->x1;
		d.y1 = r->y1;
		d.y2 = r->y1;
		draw_line(e->map_sur, d, 0x00FF00);
		r = r->next;
	}
}