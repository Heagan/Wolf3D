NAME = wolf3d

SRC =	main.c\
		read.c\
		draw.c\
		caster.c\
		keys.c

LIB = -L libft/ -lft 
INC = -I libft/ -I include

SDLLIB		=	-L C:/SDL2/lib/x64 -lSDL2main -lSDL2
SDLFLAGS	=	-I C:/SDL2/include

OBJ = $(SRC:%.c=%.o)

OBJPATH = obj/
SRCPATH = src/

SRCS = 	$(addprefix $(SRCPATH), $(SRC)) 
OBJS = 	$(addprefix $(OBJPATH), $(OBJ)) 

all: $(NAME)

$(NAME): $(OBJS)
	@gcc -o $(NAME) -mwindows $(OBJS) $(LIB) $(SDLLIB)
	@echo "Compiled $(NAME)"

$(OBJPATH)%.o: $(SRCPATH)%.c
	@echo "\033[0;32m------- COMPILLING $@ --------\033[0m"
	@mkdir -p $(OBJPATH)
	@gcc -o $@ -c $< $(INC) $(SDLFLAGS)


clean:
	rm */*.o
	rm $(NAME)

re: clean all
