#ifndef WOLF3D_H
# define WOLF3D_H

# include <fcntl.h>
# include <math.h>
# include <stdlib.h>
# include "C:/SDL2/include/SDL.h"
# include <stdio.h>
# include "libft.h"

# define SCALE 32
# define DISTANCE 10000
# define TURNRATE 5
# define MOVESPD 5
# define SCREEN_WIDTH 1920
# define SCREEN_HEIGHT 1080
# define KEYSYM e->ev.key.keysym.sym
# define DEG_TO_RAD(x) (x * M_PI) / 180
# define POV e->plr.pov
# define FOV e->plr.fov
# define SIN(x) sin(DEG_TO_RAD(x))
# define COS(x) cos(DEG_TO_RAD(x))
# define TAN(x) tan(DEG_TO_RAD(x))

typedef struct		s_draw {
	int				dx;
	int				dy;
	int				steps;
	double			xinc;
	double			yinc;
	double			x;
	double			y;
	double			x1;
	double			y1;
	double			x2;
	double			y2;
	int				i;
}					t_draw;

typedef struct		s_read
{
	int				x0;
	int				x1;
	int				y0;
	int				y1;
	struct s_read	*next;
}					t_read;

typedef struct	s_hitWall
{
	int			x;
	int			y;
	double		t;
}				t_hitWall;

typedef struct		s_plr
{
	double			x;
	double			y;
	int				fov;
	int				pov;
	double			a;
	double			s;
	int				t;
	t_hitWall		hitWall;
}					t_plr;

typedef struct		s_env
{
	t_read			*head;
	t_plr			plr;
	int				fd;
	int				map_x;
	int				map_y;
	SDL_Window		*win;
	SDL_Window		*win_map;
	SDL_Surface		*sur;
	SDL_Surface		*map_sur;
	SDL_Surface		*img;
	SDL_Renderer	*ren;
	SDL_Event		ev;
}					t_env;

void		ray_caster(t_env *e);
void		draw_wall(t_env *e, double size, int loc, double a);
int			draw_line(SDL_Surface *e, t_draw d, int colour);
void		ft_lstprint(t_env *env);
t_read		*lstnnew(int x, int y);
void		lstadd(t_read **head, int x, int y);
int			ft_read(t_env *env);
void		ft_keyhandler(t_env *env);
void		clearpixels(SDL_Surface *e);
void		sdlputpixel(SDL_Surface *e, int x, int y, int color);
void		mini_map(t_env *e);
double		collision(t_env *e);

#endif